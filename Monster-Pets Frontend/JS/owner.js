
  const url = "http://localhost:3333/owner";

  function getOwner() {
    axios
      .get(url)
      .then((response) => {
        const json = response.data;
        let tableTxt = "";

        json.forEach((owner) => {
          tableTxt += "<tr>";
          tableTxt += "<td>" + owner.id + "</td>";
          tableTxt += "<td>" + owner.owner_name + "</td>";
          tableTxt += "<td>" + owner.cpf + "</td>";
          tableTxt += "<td>" + owner.address + "</td>";
          tableTxt += "<td>" + owner.number + "</td>";
          tableTxt += "<td>" + owner.city + "</td>";
          tableTxt += "<td>" + owner.phone + "</td>";
          tableTxt += "<td>" + owner.email + "</td>";
          tableTxt +=
            "<td><button type='button' class='btn btn-warning badge-pill' data-toggle='modal' data-target='#detailsModal'>Details</button> <button type='button' class='btn btn-info badge-pill' data-toggle='modal' data-target='#editModal' >Edit</button> <button type='button' class='btn btn-danger badge-pill' data-toggle='modal' data-target='#excludeModal' id='excludeOwner'>Exclude</button></td>";
          tableTxt += "</tr>";
        });

        document.getElementById("result").innerHTML = tableTxt;
      })
      .catch((error) => console.log(error));
  }

  getOwner();

  $("btnConfirm").on("click", function addOwner() {
    let owner_name = $("#inputName").val();
    let birth = $("#inputBirth").val();
    let cpf = $("#inputCpf").val();
    let email = $("#inputEmail").val();
    let phone = $("#inputPhone").val();
    let address = $("#inputAddress").val();
    let district = $("#inputDistrict").val();
    let number = $("#inputNumber").val();
    let complement = $("#inputComplement").val();
    let city = $("#inputCity").val();
    let state = $("#inputState").val();

    axios.post(url, {
      owner_name,
      birth,
      cpf,
      email,
      phone,
      address,
      district,
      number,
      complement,
      city,
      state
    })
    .then(response => {
        alert("Owner successfully registered");
    })
    .catch(error => console.log(error));
  });


$("btnEdit").on("click", function updateOwner(id) {
    let owner_name = $("#updateName").val();
    let birth = $("#updateBirth").val();
    let cpf = $("#updateCpf").val();
    let email = $("#updateEmail").val();
    let phone = $("#updatePhone").val();
    let address = $("#updateAddress").val();
    let district = $("#updateDistrict").val();
    let number = $("#updateNumber").val();
    let complement = $("#updateComplement").val();
    let city = $("#updateCity").val();
    let state = $("#updateState").val();

    axios.put(`${url}/${id}`, {
      owner_name,
      birth,
      cpf,
      email,
      phone,
      address,
      district,
      number,
      complement,
      city,
      state
    })
    .then(response => {
        console.log(response);
    })
    .catch(error => console.log(error));
});

$("btnExclude").on("click", function deleteOwner(id) {
    axios.delete(`${url}/${id}`)
    .then(response => {
      console.log(response)
    })
    .catch(error => console.log(error));
  });
